@extends('layouts.master')
@section('content')
    @if(Session::has('message'))
        <div class="row">
            <div class="col-md-4 col-md-offset-4 success">
                {{Session::get('message')}}
            </div>
        </div>
    @endif
    <section class="row new-post">
        <div class="col-md-6 col-md-offset-3">
            <header><h3>What do you have to say?</h3></header>
            <form action="{{ route('post.store') }}" method="post">
                <div class="form-group">
                    <textarea class="form-control " name="body" id="new-post" rows="5" placeholder="Your Post"></textarea>
{{--                    @error('body')--}}
{{--                    <span class="invalid-feedback" role="alert">--}}
{{--                        <strong>{{ $message }}</strong>--}}
{{--                    </span>--}}
{{--                    @enderror--}}
                </div>
                <button type="submit" class="btn btn-primary">Create Post</button>
                <input type="hidden" value="{{ Session::token() }}" name="_token">
            </form>
        </div>
    </section>
    <section class="row posts">
        <div class="col-md-6 col-md-offset-3">
            <header><h3>What othe people say...</h3></header>
            @foreach($posts as $post)
                <article class="post">
                        <p>{{$post->body}}</p>
                    <div class="info">
                        Posted by {{ $post->user->name }} on {{ $post->created_at }}
                    </div>
                    <div class="interaction">
                        <a href="#">Like</a> |
                        <a href="#">DisLike</a> |

                        @if(Auth::user()==$post->user)
                         <form action="/post/{{$post->id}}/edit" method="post">
                             @csrf
                             @method('get')
                             <button type="submit" class="btn btn-success">Edit</button>
                         </form>
                            <form action="/post/{{$post->id}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        @endif

                    </div>
                </article>
            @endforeach
        </div>
    </section>
@endsection


