@extends('layouts.master')
@section('content')

    <section class="row new-post">
        <div class="col-md-6 col-md-offset-3">
            <header><h3>What do you have to say?</h3></header>
            <form action="/post/{{$data->id}}" method="post">
{{--            <form action="post/{{$data->id}}" method="post">--}}
                @csrf
                @method('put')
                <div class="form-group">
                    <textarea class="form-control " name="body" id="new-post" rows="5">{{$data->body}}</textarea>
                    {{--                    @error('body')--}}
                    {{--                    <span class="invalid-feedback" role="alert">--}}
                    {{--                        <strong>{{ $message }}</strong>--}}
                    {{--                    </span>--}}
                    {{--                    @enderror--}}
                </div>
                <button type="submit" class="btn btn-primary">Create Post</button>
                <input type="hidden" value="{{ Session::token() }}" name="_token">
            </form>
        </div>
    </section>
@endsection
